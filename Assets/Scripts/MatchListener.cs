using UnityEngine;
using XInputDotNetPure;

public abstract class MatchListener : MonoBehaviour {
	
	public virtual void OnHit(PlayerIndex hitPlayer, PlayerIndex playerWhoThrew) {}
	public virtual void OnMiss(float distanceToPlayer, PlayerIndex missedPlayer, PlayerIndex playerWhoThrew) {}	
	public virtual void OnFoul(PlayerIndex badPlayer) {}	
	public virtual void OnNewRound(PlayerIndex servingPlayer) {}

}
