using UnityEngine;
using XInputDotNetPure;
using System;
using System.Collections.Generic;

public abstract class MatchStatistics : MatchListener {

	private Dictionary<PlayerIndex, PlayerStats> playerStats;

	void Awake() {
		playerStats = new Dictionary<PlayerIndex, PlayerStats>();
		playerStats[PlayerIndex.One] = new PlayerStats();
		playerStats[PlayerIndex.Two] = new PlayerStats();
		playerStats[PlayerIndex.Three] = new PlayerStats();
		playerStats[PlayerIndex.Four] = new PlayerStats();
	}

	public override void OnMiss(float distanceToPlayer, PlayerIndex missedPlayer, PlayerIndex playerWhoThrew) {

	}	

	public override void OnFoul(PlayerIndex badPlayer) {

	}	

	public override void OnNewRound(PlayerIndex servingPlayer) {

	}
	
}

public class PlayerStats {
	public int hits;
	public int misses;
	public float aimingAccuracy;
}