﻿using UnityEngine;
using System.Collections;

public class HandController : MonoBehaviour {

	public float animationDuration1;
	public float animationDuration2;
	public float armLength;
	public float ballSize;

	private Transform parentTransform;
	private float timeAnimationStarted;
	private Vector3 localGrabPosition;
	private Vector3 localStartingPosition;

	// Use this for initialization
	void Start () {
		parentTransform = transform.parent.transform;
		localStartingPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - timeAnimationStarted < animationDuration1)
		{
			// Hand should move forward
			transform.localPosition = Vector3.Lerp (localStartingPosition,
			                                        localStartingPosition + localGrabPosition,
			                                   		(Time.time - timeAnimationStarted) / animationDuration1);
		}
		else if (Time.time - timeAnimationStarted < animationDuration1 + animationDuration2)
		{
			// Hand should move backward
			transform.localPosition = Vector3.Lerp (localStartingPosition + localGrabPosition,
			                                   		localStartingPosition,
			                                   		(Time.time - timeAnimationStarted - animationDuration1) / animationDuration2);
		}
	}

	public void animateHand(Vector3 ballPosition, bool ballIsInReach)
	{
		Vector3 directionVector = ballPosition - transform.position;
		directionVector = transform.InverseTransformVector(directionVector);
		// Reduce by size of the ball
		directionVector = directionVector - directionVector.normalized * ballSize;
		if (!ballIsInReach)
		{
			directionVector = directionVector.normalized * armLength;
		}
		localGrabPosition = directionVector;
		timeAnimationStarted = Time.time;
	}
}
