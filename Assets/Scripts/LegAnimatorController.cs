﻿using UnityEngine;
using System.Collections;

public class LegAnimatorController : MonoBehaviour {

	public float angleDifference;

	private Rigidbody2D parentRigidbody;
	private Animator legAnimator;
	//private Animation legAnimation;

	void Start () {
		parentRigidbody = transform.parent.rigidbody2D;
		legAnimator = transform.GetComponent<Animator>();
		//legAnimation = transform.GetComponent<Animation>();
	}
	
	void Update () {
		if (parentRigidbody.velocity.magnitude < 2)
		{
			legAnimator.SetInteger("doWalk", 0);
		}
		else
		{
			legAnimator.speed = parentRigidbody.velocity.magnitude / 5;

			// Turn legs to walking direction:
			float targetLegAngle = Mathf.Atan2(parentRigidbody.velocity.y,
			                                parentRigidbody.velocity.x)
										* Mathf.Rad2Deg + 270;
			float currentBodyAngle = parentRigidbody.transform.rotation.eulerAngles.z;
			angleDifference = (currentBodyAngle - targetLegAngle + 360) % 360;
			if (angleDifference > 180)
			{
				legAnimator.SetInteger("doWalk", 2); // Forwards
			}
			else
			{
				targetLegAngle += 180;
				legAnimator.SetInteger("doWalk", 1); // Backwards
			}
			transform.rotation = Quaternion.Euler(new Vector3(0, 0, targetLegAngle));
		}
	}
}
