using UnityEngine;

/* 
 * "Give me ooooone moment in time"
 * A little struct to store/collect some data 
 * regarding the ball physics at a certain time.
 */
public class BallMoment 
{
	public BallMoment() {
		this.time = Time.time;
	}
	public float time {get; set;}
	public Vector2 velocity {get; set;}
	public Vector2 position {get; set;}
	public float distance {get; set;} // TODO: remove this, use position instead
	public bool charged {get; set;}
}
