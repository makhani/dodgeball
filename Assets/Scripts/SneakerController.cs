﻿using UnityEngine;
using System.Collections;

public class SneakerController : MonoBehaviour {

	public bool movesRight;
	public float movementSpeed;
	public bool isFirst;
	public float firstOffset;

	private Vector3 startingPosition;

	// Use this for initialization
	void Start () {
		startingPosition = transform.position;

		int algebraicSign = movesRight ? 1 : -1;
		rigidbody2D.velocity = transform.right * movementSpeed * algebraicSign;
		if (isFirst)
		{
			transform.position += transform.right * firstOffset * algebraicSign;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x > 28 && movesRight || transform.position.x < -28)
		{
			transform.position = startingPosition;
		}
	}
}
