﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class BallInteractionController : MonoBehaviour {

	public PlayerIndex playerIndex;
	public float   ballLerpDuration;
	public float   catchAttemptCooldown = 0.7f;
	public float   standardThrowingForce = 30;
	[Range(0,1)]
	public float   distanceToTimeImpactRatio = 0.5f;
	public float   chargedBallVelocityImpact = 0.6f;
	[Range(1,5)]
	public float   bonusAmplifier = 1;
	public float   reactionBonusTimeFrame = 1;
	public float   distanceBonusLength = 0.25f; // TODO: Don't set this in the editor, get it from collider bounds instead.
	public float   thumbstickDeadzoneSize;
	public float   holdBallOffset;
	public bool    ballIsInReach = false;
	public float   reachRadius;
	public float   rumbleDuration;
	[HideInInspector]
	public bool 	allowBallInteraction;

	private bool    playerHasBall;
	private float 	highRumbleAmount;
	private float   lowRumbleAmount;
	private Ball    ball;
	private float   lastCatchAttempt;
	private float   lastCatchTime;
	private float   lastThrow;
	private float   lastBonusFactor;
	private Vector2 throwingDirection;
	private Vector2 throwingVelocity;
	private bool    catchButtonDown = false;
	private bool    catchButtonPressed = false;
	private bool    catchButtonUp = false;
	private float   spin;
	private float   lastHit;
	private Rigidbody2D ballRigidbody;
	private GamePadState input;
	private GamePadState prevInput;
	private BallMoment catchMoment;
	private HumanNoiseController humanNoiseController;
	private MatchController matchController;
	private MovementController movementController;
	private HandController leftHandController;
	private HandController rightHandController;
	//private RotateCharacterController rotateCharacterController;

	void Awake() {
		// Get object references
		ballRigidbody = GameObject.FindGameObjectWithTag("Ball").rigidbody2D;
		var matchControllerObject = GameObject.FindGameObjectWithTag("MatchController");
		var leftHand = transform.FindChild("Left Hand").gameObject;
		var rightHand = transform.FindChild("Right Hand").gameObject;

		// Get scripts
		ball = ballRigidbody.gameObject.GetComponent<Ball>();
		movementController = gameObject.GetComponent<MovementController>();
		//rotateCharacterController = gameObject.GetComponent<RotateCharacterController>();
		matchController = matchControllerObject.GetComponent<MatchController>();
		leftHandController = leftHand.GetComponent<HandController>();
		rightHandController = rightHand.GetComponent<HandController>();
		humanNoiseController = this.GetComponent<HumanNoiseController>();
	}

	// Use this for initialization
	void Start ()
	{
		// Set default values
		playerHasBall = false;
		lastCatchAttempt = Time.time;
		lastBonusFactor = 0;
		lastHit = 0;
		lastThrow = 0;
		allowBallInteraction = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		var input = GamePad.GetState(playerIndex, GamePadDeadZone.Circular);
		var takeButtonDown = input.Buttons.A == ButtonState.Pressed && prevInput.Buttons.A == ButtonState.Released;
		var takeButtonPressed = input.Buttons.A == ButtonState.Pressed;
		var takeButtonUp = input.Buttons.A == ButtonState.Released && prevInput.Buttons.A == ButtonState.Pressed;

		// The player takes the ball
		catchButtonDown = takeButtonDown ? true : catchButtonDown;
		if(catchButtonDown && allowBallInteraction) 
		{
			bool cooldownIsOver = (Time.time - lastCatchAttempt) > catchAttemptCooldown;
			if (cooldownIsOver)
			{
				lastCatchAttempt = Time.time;
				leftHandController.animateHand(ballRigidbody.transform.position, ballIsInReach);
				rightHandController.animateHand(ballRigidbody.transform.position, ballIsInReach);
			}
			if(ballIsInReach && cooldownIsOver) 
			{
				Vector3 ballDistance = transform.position - ballRigidbody.transform.position;

				// Track ball's physics data at the moment of catching.
				catchMoment = new BallMoment();
				catchMoment.distance = ballDistance.magnitude;
				catchMoment.velocity = ballRigidbody.velocity;
				catchMoment.charged = ball.charged;

				playerHasBall = true;
				movementController.allowMovement = false;
				lastCatchTime = Time.time;

				highRumbleAmount = (calculateBonusFactor() - 1) / 1.5f;
			}
		}

		// Player holds the ball
		catchButtonPressed = takeButtonPressed ? true : catchButtonPressed;
		if(catchButtonPressed) 
		{
			if(playerHasBall) 
			{
				// First, we make the player look in the aiming-direction.
				var verticalAxis = input.ThumbSticks.Left.Y;
				var horizontalAxis = input.ThumbSticks.Left.X;
				var directionVector = new Vector2(horizontalAxis, verticalAxis);

				// Apply radial deadzone:
				bool isInDeadzone = (directionVector.magnitude < thumbstickDeadzoneSize);

				if (!isInDeadzone)
				{
					float angle = Mathf.Atan2(verticalAxis, horizontalAxis) * Mathf.Rad2Deg + 180;
					// Use this for rotation with speed limit:
					//rotateCharacterController.rotateTowards(angle);
					// Use this instantaious rotation:
					transform.rotation = Quaternion.Euler (new Vector3(0, 0, angle));
					// TODO: Decide what we want here.
				}

				putBallInFrontOfPlayer(ballLerpDuration);
			}
		}

		// Player throws
		catchButtonUp = takeButtonUp ? true : catchButtonUp;
		if(catchButtonUp) 
		{
			if(playerHasBall) 
			{
				// Get direction
				var directionAngle = (transform.rotation.eulerAngles.z + 180) * Mathf.Deg2Rad;
				throwingDirection = new Vector2(Mathf.Cos(directionAngle), Mathf.Sin (directionAngle));

				// This is the minimum velocity
				Vector2 minimumVelocity = throwingDirection * standardThrowingForce;

				float bonusFactor = calculateBonusFactor();
				Debug.Log("Bonus factor: " + bonusFactor);

				// Set rumble
				lowRumbleAmount = bonusFactor - 1;

				// Apply bonus factor to minimum throwing velocity.
				throwingVelocity = minimumVelocity * bonusFactor;

				// Get spin
				if(input.Triggers.Left > 0) 
				{
					spin = -input.Triggers.Left;
				}
				else 
				{
					spin = input.Triggers.Right;
				}

				// Save information about who threw the ball
				ball.whoThrewLast = playerIndex;

				// Remove movement lock
				movementController.allowMovement = true;

				// Prevent the player from instantly catching the ball after throwing it
				lastCatchAttempt = Time.time;
				lastThrow = Time.time;
				lastBonusFactor = bonusFactor;
			}
		}

		// Do the rumble
		float highRumbleFactor = (lastCatchTime + rumbleDuration - Time.time) / rumbleDuration;
		//float lowRumbleFactor = (lastThrow + rumbleDuration - Time.time) / rumbleDuration;
		float lowRumbleFactor = (lastThrow + rumbleDuration - Time.time) > 0 ? ((lastBonusFactor - 1) / bonusAmplifier) : 0;
		float highRumbleValue = Mathf.Max(highRumbleAmount * highRumbleFactor, 0);
		float lowRumbleValue = Mathf.Max(lowRumbleAmount * lowRumbleFactor, 0);
		GamePad.SetVibration(playerIndex, lowRumbleValue, highRumbleValue);
		//Debug.Log ("time:" + Time.time + "Rumble:" + lowRumbleFactor + ", " + highRumbleFactor);

		prevInput = input;


	}

	IEnumerator Rumble(float lowFrequencyStrength, float highFrequencyStrength, float duration) {
		float t = Time.time;
		float end = Time.time + duration;
		while(t < end) {
			GamePad.SetVibration(playerIndex, lowFrequencyStrength, highFrequencyStrength);
			yield return null;
		}
	}
	

	void FixedUpdate() 
	{
		// Calculate if the ball is in reach:
		Vector3 centerOfReachableArea = transform.TransformPoint(-1 * reachRadius, 0, 0);
		Vector3 distanceVector = centerOfReachableArea - ballRigidbody.transform.position;
		float ballDistanceToCenterOfReachableArea = distanceVector.magnitude;
		ballIsInReach = (ballDistanceToCenterOfReachableArea < reachRadius);

		if(catchButtonDown) 
		{
			catchButtonDown = false;
		}
		if(catchButtonPressed) 
		{
			if(playerHasBall) 
			{
				// Ball is being lerped to the player :D
				ballRigidbody.velocity = new Vector2(0,0);
			}
			catchButtonPressed = false;
		}
		if(catchButtonUp) 
		{	
			if(playerHasBall) 
			{
				// To prevent the ball from being behind the player (during lerp)
				putBallInFrontOfPlayer(0);

				// Throw the ball by immediatly changing it's velocity. 
				ballRigidbody.velocity = throwingVelocity;
				ball.spin = spin;
				ball.numberOfWallTouches = 0;
				Debug.Log (spin);

				// Player has no ball anymore.
				playerHasBall = false;

				humanNoiseController.PlayThrowingNoise(throwingVelocity);
			}
			catchButtonUp = false;
		}
	}

	void OnTriggerEnter2D()
	{
		// Maybe replace this magic number (0.5). On the other hand it's not likely to change...
		//Debug.Log (ball.whoThrewLast != playerIndex);
		//Debug.Log (Time.time - lastHit > 0.5);
		if(ball.charged 
		   && ball.whoThrewLast != playerIndex 
		   && Time.time - lastHit > 0.5
		   && Time.time - lastThrow > 0.15
		   && !playerHasBall)
		{
			// register hit
			matchController.RegisterHit(this.playerIndex, ball.whoThrewLast);
			humanNoiseController.PlayHitNoise();
			lastHit = Time.time;

			// TODO: Handle rumble in a nicer way
			highRumbleAmount = 1;
			lowRumbleAmount = 1;
			lastThrow = Time.time;
			lastCatchTime = Time.time;
			allowBallInteraction = false;
		}
		else {
			//Debug.Log ("This!");
		}
	}

	float calculateBonusFactor()
	{
		// Resolve impacts of different factors
		var timeImpact = distanceToTimeImpactRatio;
		var distanceImpact = 1 - distanceToTimeImpactRatio;
		
		// Determine how long the player held the ball
		float holdingTime = Time.time - catchMoment.time;
		float timeFactor = 0;
		
		// If the player was quick enough (inside time frame)
		// we calculate a factor based on the amount of time used
		if(holdingTime < reactionBonusTimeFrame) {
			// this is basically a percentage value.
			timeFactor = (reactionBonusTimeFrame-holdingTime) / reactionBonusTimeFrame;
		}
		
		// Calculate a distance bonus amount/factor
		float distanceFactor = 0;
		if(catchMoment.distance < distanceBonusLength) {
			distanceFactor = catchMoment.distance / distanceBonusLength;
		}
		
		this.throwingVelocity = Vector2.zero;

		// This factor determines the quality/skill of the catch / throw
		var skillAmount = distanceImpact * distanceFactor + timeImpact * timeFactor; // 0 to 100 Percent

		// Transfer some of the charged balls velocity to the new throw.
		if(catchMoment.charged) {
			skillAmount += catchMoment.velocity.magnitude * chargedBallVelocityImpact;
		}

		// Make the factor 100% and more, and scale/amplify it.
		var bonusFactor = 1 + (skillAmount * bonusAmplifier);

		return bonusFactor;
	}

	void putBallInFrontOfPlayer(float duration)
	{
		float lerpValue = 1;
		if (duration > 0)
		{
			lerpValue = (Time.time - lastCatchTime) / duration;
		}

		var newBallPosition = transform.TransformPoint(-1 * holdBallOffset, 0, 0);
		ballRigidbody.transform.position = Vector2.Lerp (ballRigidbody.transform.position,
		                                                 newBallPosition,
		                                                 lerpValue);
	}
}
