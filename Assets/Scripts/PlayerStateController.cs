using UnityEngine;
using XInputDotNetPure;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class PlayerStateController : MatchListener {

	private PlayerMoment startState;

	void Start() 
	{
		startState = new PlayerMoment();
		startState.position = rigidbody2D.position;
		startState.rotation = transform.rotation;
		startState.velocity = rigidbody2D.velocity;
		startState.allowMovement = true;
		startState.allowBallInteraction = true;
	}

	void ResetState() {
		Debug.Log ("Resetting!");
		rigidbody2D.position = startState.position;
		transform.rotation = startState.rotation;
		rigidbody2D.velocity = startState.velocity;
		this.GetComponent<MovementController>().allowMovement = startState.allowMovement;
		this.GetComponent<BallInteractionController>().allowBallInteraction = startState.allowBallInteraction;
	}

	public override void OnNewRound(PlayerIndex servingPlayer) {
		ResetState();
	}
}

