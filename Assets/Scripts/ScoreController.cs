﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public class ScoreController : MatchListener {

	public PlayerIndex player;
	private string playerPrefix;
	private int score;
	private UnityEngine.UI.Text guiScore;

	// Use this for initialization
	void Start () 
	{
		guiScore = this.GetComponent<UnityEngine.UI.Text>();
		score = 0;
	}

	public override void OnHit(PlayerIndex playerIndex, PlayerIndex playerWhoThrew) 
	{
		// TODO: For 2vs2 we need a team variable.
		if(playerIndex != this.player) {
			score++;
			guiScore.text = score.ToString();
		}
	}

}
