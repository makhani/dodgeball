﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RainbowScript : MonoBehaviour {

	public Text text;
	public Color refColor;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		text.color = Color.magenta;
	}
	
	// Update is called once per frame
	void Update () {
		text.color = text.color;
		if (text.color == Color.magenta)
			refColor = Color.cyan;
		if (text.color == Color.cyan)
			refColor = Color.magenta;

		text.color = Color.Lerp (text.color, refColor, 0.5f);


	}
}
