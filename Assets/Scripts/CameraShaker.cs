﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public class CameraShaker : MatchListener {

	public float duration;
	public float positionMagnitude;
	public float rotationMagnitude;

	public override void OnHit(PlayerIndex hitPlayer, PlayerIndex playerWhoThrew) {
		StartCoroutine("Shake");
	}
	
	public IEnumerator Shake() {
		// inspired by http://unitytipsandtricks.blogspot.de/2013/05/camera-shake.html
		float elapsed = 0.0f;
		Vector3 originalCamPos = Camera.main.transform.position;
		Quaternion originalCamRot = Camera.main.transform.rotation;
		while (elapsed < duration) {
			elapsed += Time.deltaTime;
			float percentComplete = elapsed / duration;
			float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;

			// map value to [-1, 1]
			float angle = Random.value * 2.0f - 1.0f;

			angle *= rotationMagnitude * damper;
			x *= positionMagnitude * damper;
			y *= positionMagnitude * damper;

			Vector3 rot = new Vector3(0,0,angle);
			Camera.main.transform.position = new Vector3(x, y, originalCamPos.z);
			Camera.main.transform.rotation = Quaternion.Euler(rot);
			yield return null;
		}
		Camera.main.transform.position = originalCamPos;
		Camera.main.transform.rotation = originalCamRot;
	} 
}
