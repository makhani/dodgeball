﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class WalkingSoundController : MonoBehaviour {

	public float minimumSpeed = 1;
	public float speedThresholdForBeingFast = 6;
	public AudioClip[] slowWalking;
	public AudioClip[] fastWalking;
	public float fadeTime;
	private float currentFadeTime;
	private bool fadeIn;
	private Rigidbody2D playerBody;
	private AudioSource audioComponent;
	private AudioClip currentClip;
	private bool isFast;
	private bool isPlaying;

	// Use this for initialization
	void Start () {
		playerBody = gameObject.GetComponent<Rigidbody2D>();
		audioComponent = gameObject.AddComponent<AudioSource>();
		audioComponent.rolloffMode = audio.rolloffMode; // Copy from first audio source.
		audioComponent.loop = false;
	}
	
	// Update is called once per frame
	void Update () {
		var currentSpeed = playerBody.velocity.magnitude;

		// Player has to be moving in order to play any sound
		if(currentSpeed > minimumSpeed) 
		{
			var playerChangedSpeedCategory = false;
			AudioClip[] soundClips;
			if(currentSpeed > speedThresholdForBeingFast) {
				//Debug.Log ("Speed it up");
				soundClips = fastWalking;
				if(!isFast) {
					playerChangedSpeedCategory = true;
				}
				isFast = true;
			}
			else {
				//Debug.Log ("Slow it down");
				soundClips = slowWalking;
				if(isFast) {
					playerChangedSpeedCategory = true;
				}
				isFast = false;
			}
			// Select and start new clip either if (old one has finished-nah!) or speed category changed
			if(playerChangedSpeedCategory) {
				//Debug.Log ("Time for a change");
				var randomIndex = Random.Range(0, soundClips.Length);
				audioComponent.clip = soundClips[randomIndex];
				var randomStart = Random.Range(0, audioComponent.clip.length);
				audioComponent.time = randomStart;
				audioComponent.Play();
				isPlaying = true;
				//StartFadeIn();
			}
			if(isPlaying == false) {
				audioComponent.time = 0f;
				audioComponent.Play();
				isPlaying = true;
				StartFadeIn();
			}

			// TODO: This has to be replaced with something more reliable.
			isPlaying = audioComponent.isPlaying;
		}
		else
		{
			if(isPlaying == true) {
				audioComponent.Pause();
				isPlaying = false;
			}
		}

		if(fadeIn) {
			//increment timer once per frame
			currentFadeTime += Time.deltaTime;
			if (currentFadeTime > fadeTime) {
				currentFadeTime = fadeTime; // cap value.
				fadeIn = false;
			}
			float t = currentFadeTime / fadeTime;
			t = t*t * (3f - 2f*t); // smoothing.
			audioComponent.volume = Mathf.Lerp(0, 1, t);
			Debug.Log(audioComponent.volume);
		}
	}

	void StartFadeIn() {
		currentFadeTime = 0;
		fadeIn = true;
	}
}
