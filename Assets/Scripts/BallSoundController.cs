﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class BallSoundController : MonoBehaviour {

	public float softToSoftmidThreshold;
	public float softmidToMidThreshold;
	public float midTomidHardThreshold;
	public float midhardToHardThreshold;
	public AudioClip[] softRebounds;
	public AudioClip[] softmidRebounds;
	public AudioClip[] midRebounds;
	public AudioClip[] midhardRebounds;
	public AudioClip[] hardRebounds;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlaySound(float speed, bool charged) {
		AudioClip[] soundClips;
		if(speed < softToSoftmidThreshold) {
			Debug.Log ("soft rebound");
			soundClips = softRebounds;
		}
		else if (speed < softmidToMidThreshold) {
			Debug.Log ("soft-mid rebound");
			soundClips = softmidRebounds;
		}
		else if(speed < midTomidHardThreshold) {
			Debug.Log ("mid rebound");
			soundClips = midRebounds;
		}
		else if(speed < midhardToHardThreshold) {
			Debug.Log ("mid-hard rebound");
			soundClips = midhardRebounds;
		}
		else {
			Debug.Log ("hard rebound");
			soundClips = hardRebounds;
		}

		var randomIndex = Random.Range(0, soundClips.Length);
		var clip = soundClips[randomIndex];
		audio.PlayOneShot(clip);
	}
}
