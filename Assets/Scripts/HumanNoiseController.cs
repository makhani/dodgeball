﻿using UnityEngine;
using System.Collections;

public class HumanNoiseController : MonoBehaviour {

	[Range(0,1)]
	public float volume = 0.4f;
	public AudioClip[] painSounds;
	public AudioClip[] throwingSounds;
	
	// Use this for initialization
	void Start () {
		audio.volume = volume;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayHitNoise() {
		var randomIndex = Random.Range(0, painSounds.Length);
		audio.PlayOneShot(painSounds[randomIndex]);
	}

	public void PlayThrowingNoise(Vector2 velocity) {
		// Play random throwing sound
		var randomIndex = Random.Range(0, throwingSounds.Length);
		audio.PlayOneShot(throwingSounds[randomIndex]);
	}
}
