﻿using UnityEngine;
using System.Collections;

public class RotateCharacterController : MonoBehaviour {

	public float maxRotationSpeed; // Degrees per second
	public float minBallDistance;
	private Transform ballTransform;

	void Awake ()
	{
		ballTransform = GameObject.FindGameObjectWithTag("Ball").transform;
	}

	void Update ()
	{
		var ballDistance = transform.position - ballTransform.position;
		if (ballDistance.magnitude > minBallDistance)
		{
			float targetAngle = Mathf.Atan2(ballTransform.position.y - transform.position.y,
			                                ballTransform.position.x - transform.position.x)
				* Mathf.Rad2Deg + 180;
            rotateTowards(targetAngle);
		}
	}
	
	public void rotateTowards (float targetAngle)
	{
		float currentAngle = transform.rotation.eulerAngles.z;
		float newAngle = targetAngle;
		
		if (Mathf.Abs(currentAngle - targetAngle) > maxRotationSpeed * Time.deltaTime)
		{
			// Determine in which direction to turn:
			bool clockwise = true;
			if (targetAngle > currentAngle)
			{
				clockwise = (targetAngle - currentAngle < 180);
			}
			else
			{
				clockwise = (currentAngle - targetAngle > 180);
			}
			
			int algebraicSign = clockwise ? 1 : -1;
			newAngle = currentAngle + (maxRotationSpeed * Time.deltaTime * algebraicSign);
		}
		
		transform.rotation = Quaternion.Euler(new Vector3(0, 0, newAngle));
	}
}
