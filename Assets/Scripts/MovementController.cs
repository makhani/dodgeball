﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class MovementController : MatchListener {

	public PlayerIndex playerIndex;
	public float accelerationFactor;
	public float breakFactor;
	public float deadzoneSize;
	[HideInInspector]
	public bool allowMovement;
	private Vector2 movement;

	// Use this for initialization
	void Start () 
	{
		allowMovement = true;
		//playerPrefix = System.Enum.GetName(typeof(Player), player);
	}
	
	// Update is called once per frame
	void Update () 
	{
		movement = new Vector2(0,0);

		if(allowMovement) 
		{
			var state = GamePad.GetState(playerIndex, GamePadDeadZone.Circular);
			float horizontal = state.ThumbSticks.Left.X;
			float vertical = state.ThumbSticks.Left.Y;
			movement = new Vector2(horizontal, vertical);

			// Apply radial deadzone:
			movement = movement.magnitude < deadzoneSize ? Vector2.zero : movement;
		}
	}

	void FixedUpdate() 
	{
		if (movement.magnitude != 0)
		{		
			rigidbody2D.AddForce(movement * accelerationFactor);
		}
		else 
		{
			rigidbody2D.AddForce(-1 * rigidbody2D.velocity * breakFactor);
		}
	}

	public override void OnHit(PlayerIndex playerWhoGotHit, PlayerIndex playerWhoThrew) {
		if(playerWhoGotHit == playerIndex)
			allowMovement = false;
	}
}


public enum Player 
{
	Player1 = 1,
	Player2 = 2
}
