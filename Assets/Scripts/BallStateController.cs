using UnityEngine;
using XInputDotNetPure;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class BallStateController : MatchListener {
	
	private BallMoment startState;
	public float xOffsetFromCenterOnServe = 4.1f;
	
	void Start() 
	{
		startState = new BallMoment();
		startState.position = rigidbody2D.position;
		startState.velocity = rigidbody2D.velocity;
		startState.charged = false;
	}
	
	void ResetState(PlayerIndex servingPlayer) {
		var x = servingPlayer == PlayerIndex.One ? -xOffsetFromCenterOnServe : xOffsetFromCenterOnServe;
		rigidbody2D.position = new Vector2(x, -0.01f);
		rigidbody2D.velocity = startState.velocity;
		this.GetComponent<Ball>().charged = startState.charged;
		this.GetComponent<Ball>().numberOfWallTouches = 1;
	}
	
	public override void OnNewRound(PlayerIndex servingPlayer) {
		ResetState(servingPlayer);
	}
}
