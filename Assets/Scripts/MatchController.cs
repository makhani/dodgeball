﻿using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;
using System.Collections;
using System.Collections.Generic;

/*
 * This class handles all the rules and meta gameplay. 
 */
public class MatchController : MonoBehaviour {

	public int winningScore;
	public float hitToCountInDuration;
	public float countInDuration;
	public Text bigText;
	public GameObject statsPanel;
	public GameObject[] registeredMatchListenerObjects;
	public AudioClip[] SoundtrackClipsPerRound;
	public AudioClip StatisticsSoundtrack;
	[Range (0,1)]
	public float volume = 0.3f;
	private int soundtrackIndex;
	private ArrayList matchListeners = new ArrayList();
	private AudioSource source;
	private Dictionary<PlayerIndex, int> scores; 

	void Awake () {
		scores = new Dictionary<PlayerIndex, int>();
		scores[PlayerIndex.One] = 0;
		scores[PlayerIndex.Two] = 0;
		scores[PlayerIndex.Three] = 0;
		scores[PlayerIndex.Four] = 0;
	}

	// Use this for initialization
	void Start () 
	{
		// Extract match listeners.
		for(var i = 0; i < registeredMatchListenerObjects.Length; i++) 
		{
			var scripts = registeredMatchListenerObjects[i].GetComponents<MonoBehaviour>();
			foreach(var script in scripts) 
			{
				if(script is MatchListener) 
				{
					matchListeners.Add(script);
				}
			}
		}
		source = gameObject.AddComponent<AudioSource>();
		source.loop = true;
		source.volume = volume;
		soundtrackIndex = 0;
		bigText.enabled = false;
		statsPanel.SetActive(false);
	}

	public void RegisterHit(PlayerIndex hitPlayer, PlayerIndex playerWhoThrew) 
	{
		// notify all observers.
		foreach(MatchListener ml in matchListeners) {
			ml.OnHit(hitPlayer, playerWhoThrew);
		}
		scores[playerWhoThrew] += 1;
		if(scores[playerWhoThrew] < winningScore) {
			StartCoroutine(WaitAndStartNewRound(hitPlayer));
			StartCoroutine(WaitAndDisplayCountIn());
		}
		else {
			bigText.text = "Player " + playerWhoThrew + " won!";
			bigText.enabled = true;
			StartCoroutine(WaitAndDisplayStats());
		}
		source.Stop();
	}

	public void RegisterMiss(float distanceToMissedPlayer, PlayerIndex missedPlayer, PlayerIndex playerWhoThrew) 
	{
		//Debug.Log ("Oooohh, that was close! (" + distanceToMissedPlayer + ")");
		foreach(MatchListener ml in matchListeners) {
			ml.OnMiss(distanceToMissedPlayer, missedPlayer, playerWhoThrew);
		}
	}

	public void RegisterFoul(PlayerIndex playerWhoFouledIndex) 
	{
		Debug.Log ("Bad play!");
		foreach(MatchListener ml in matchListeners) {
			ml.OnFoul(playerWhoFouledIndex);
		}
	}

	public IEnumerator WaitAndStartNewRound(PlayerIndex servingPlayer) {
		Debug.Log ("New Round!");
		yield return new WaitForSeconds(hitToCountInDuration + countInDuration);
		foreach(MatchListener ml in matchListeners) {
			ml.OnNewRound(servingPlayer);
		}
		Debug.Log ("...now!");
		source.clip = SoundtrackClipsPerRound[soundtrackIndex];
		source.Play();
		soundtrackIndex++;
		bigText.enabled = false;
	}

	public IEnumerator WaitAndDisplayCountIn() {
		yield return new WaitForSeconds(hitToCountInDuration);
		bigText.enabled = true;
		bigText.text = "3";
		yield return new WaitForSeconds(countInDuration/3);
		bigText.text = "2";
		yield return new WaitForSeconds(countInDuration/3);
		bigText.text = "1";
	}

	public IEnumerator WaitAndDisplayStats() {
		yield return new WaitForSeconds(1);
		bigText.enabled = false;
		statsPanel.SetActive(true);
		source.PlayOneShot(StatisticsSoundtrack);
	}

	public void RestartMatch() {
		Application.LoadLevel (Application.loadedLevelName);
	}

	public void BackToMainMenu () {
		Application.LoadLevel ("main_menu");
	}
}
