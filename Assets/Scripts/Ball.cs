﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public class Ball : MonoBehaviour {

	public GameObject MatchControllerObject;
	public float chargedThreshold;
	public ParticleSystem chargedParticles;
	[HideInInspector]
	public bool charged;
	[HideInInspector]
	public float spin;
	public float spinFactor = 2;
	public float curveForceFactor = 3;
	public int numberOfWallTouches;
	public int wallTouchesWhenThrown;
	public PlayerIndex whoThrewLast;
	public LayerMask playerLayer;
	
	private MatchController matchController;
	private BallSoundController ballSoundController;
	private bool detectMisses;

	// Use this for initialization
	void Start () 
	{
		matchController = MatchControllerObject.GetComponent<MatchController>();
		ballSoundController = this.GetComponent<BallSoundController>();
		var zPos = chargedParticles.transform.localPosition.z;
		chargedParticles.transform.localPosition = new Vector3(0,0,zPos);
		detectMisses = true;
		numberOfWallTouches = 1;
		charged = false;
		//chargedParticles.Clear();
	}

	// Update is called once per frame
	void Update () 
	{
		if(charged) 
		{
			//chargedParticles.Emit(200);
			chargedParticles.Play();
		}
		else 
		{
			chargedParticles.Stop();
		}
	
	}

	void FixedUpdate () 
	{
		charged = (numberOfWallTouches == 0 &&
		           rigidbody2D.velocity != Vector2.zero);

		if(charged) 
		{
			if(detectMisses) 
			{
				// Detect misses (almost hits)
				var flyingDirection = this.rigidbody2D.velocity.normalized;
				var toTheLeft = new Vector2(flyingDirection.y, -flyingDirection.x);
				var toTheRight = new Vector2(-flyingDirection.y, flyingDirection.x);
				
				RaycastHit2D hit = Physics2D.Raycast(this.transform.position, toTheLeft, 4, playerLayer.value);
				if(hit.collider == null) 
				{
					hit = Physics2D.Raycast(this.transform.position, toTheRight, 4, playerLayer.value);
				}
				
				if (hit.collider != null) 
				{
					if (hit.transform.tag.Equals("Player")) 
					{
						PlayerIndex missedPlayer = hit.transform.GetComponent<MovementController>().playerIndex;
						PlayerIndex playerWhoThrew = this.whoThrewLast;
						matchController.RegisterMiss(hit.distance, missedPlayer, playerWhoThrew);
						detectMisses = false; // don't detect this player again as long as this ball is charged.
					}
				}
			}
			
		} 
		else
		{
			detectMisses = true;
		}

		// Curve ball stuff
		if(this.spin != 0) {

			// In reality the spin is even faster if the ball was hit harder (which in our case is == velocity)
			float spinByVelocity = this.rigidbody2D.velocity.magnitude * this.spin;
			
			// Make the ball spin (note: this doesn't affect the flying curve)
			this.rigidbody2D.AddTorque(spinByVelocity * spinFactor);
			
			// Now we make alter the flying curve, by adding a constant force at 90 or -90 degrees of flying direction.
			var curveForceDirection = Vector2.zero;
			var flyingDirection = this.rigidbody2D.velocity;

			if(this.spin > 0)
				curveForceDirection = new Vector2(flyingDirection.y, -flyingDirection.x).normalized;
			else 
				curveForceDirection = new Vector2(-flyingDirection.y, flyingDirection.x).normalized;

			this.rigidbody2D.AddForce(curveForceDirection * curveForceFactor * Mathf.Abs(spinByVelocity));
		}

	}

	public void OnWallHit() {
		// TODO: I think the velocity might already be affected by the wall hit, so a buffered solution may be more reliable.
		numberOfWallTouches++;
		ballSoundController.PlaySound(rigidbody2D.velocity.magnitude, this.charged);
	}


}
