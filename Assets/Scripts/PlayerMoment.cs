using UnityEngine;

/* 
 * "Give me ooooone moment in time"
 */
public class PlayerMoment 
{
	public PlayerMoment() {
		this.time = Time.time;
	}
	public float time {get; set;}
	public Vector2 velocity {get; set;}
	public Quaternion rotation {get; set;}
	public Vector2 position {get; set;}
	public bool allowMovement {get; set;}
	public bool allowBallInteraction {get; set;}
}
