﻿using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour {

	public Rigidbody2D ballRigidbody;
	[Range (1, 10)]
	public float ballSpinReduction = 2;

	private Ball ball;

	// Use this for initialization
	void Start ()
	{
		// Get ball script
		ball = ballRigidbody.gameObject.GetComponent<Ball>();
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	void OnTriggerEnter2D()
	{
		ball.OnWallHit();

		// Reduce ball spin
		ball.spin = ball.spin / ballSpinReduction;
		if(Mathf.Abs(ball.spin) < 0.05) {
			ball.spin = 0;
		}
	}
}
